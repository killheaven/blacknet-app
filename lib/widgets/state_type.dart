
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/generated/i18n.dart';

class StateLayout extends StatefulWidget {
  
  const StateLayout({
    Key key,
    @required this.type,
    this.hintText
  }):super(key: key);
  
  final StateType type;
  final String hintText;

  static empty([String msg]){
    if(msg == null){
      msg = "No Datas";
    }
    return new ListTile(
      title: Center(child: Text(msg))
    );
  }
  
  @override
  _StateLayoutState createState() => _StateLayoutState();
}

class _StateLayoutState extends State<StateLayout> {
  
  String _img;
  String _hintText;
  
  @override
  Widget build(BuildContext context) {
    switch (widget.type){
      case StateType.network:
        _img = "wifi";
        _hintText = "";
        break;
      case StateType.loading:
        _img = "";
        _hintText = "";
        break;
      case StateType.empty:
        _img = "";
        _hintText = "";
        break;
    }
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          widget.type == StateType.loading ? const CupertinoActivityIndicator(radius: 16.0) :
          (widget.type == StateType.empty ? StateLayout.empty(S.of(context).noData): 
          Icon(Icons.signal_wifi_off, size: 64, color: Theme.of(context).accentColor,)),
          // Opacity(
          //   opacity: 1,
          //   child: Container(
          //     height: 120.0,
          //     width: 120.0,
          //     decoration: BoxDecoration(
          //       image: DecorationImage(
          //         image: ImageUtils.getAssetImage("state/$_img"),
          //       ),
          //     ),
          //   ))
          // ),
          Gaps.vGap16,
          Text(
            widget.hintText ?? _hintText,
            style: Theme.of(context).textTheme.subtitle.copyWith(fontSize: 14.0),
          ),
          Gaps.vGap50,
        ],
      ),
    );
  }
}

enum StateType {
  /// 无网络
  network,
  /// 加载中
  loading,
  /// 空
  empty,
  // nodata

}