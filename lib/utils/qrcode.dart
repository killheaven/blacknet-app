import 'package:majascan/majascan.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/utils/toast.dart';
import 'package:blacknet/utils/validator.dart';
import 'package:blacknet/res/index.dart';

class QrcodeHelper {
  static Future<String>  scanQrAddress() async {

    String barcodeScanRes;

    try { 
      barcodeScanRes = await MajaScan.startScan(
        title: "QRcode Scanner", 
        barColor: Colors.transparent, 
        titleColor: Colours.grey, 
        qRCornerColor: Colours.gold,
        qRScannerColor: Colours.gold,
        flashlightEnable: true
      );
    } catch (e) {
      Toast.show("扫描失败");
    }

    if (barcodeScanRes == null) return null;

    List<String> ss = barcodeScanRes.split(":");

    if(validatorAddress(ss[1])){
      return ss[1];
    }
    
    return null;
  }
}