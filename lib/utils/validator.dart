
bool validatorPassword(String password){
  if(password.isEmpty || password.length < 8){
    return false;
  }
  return true;
}

bool validatorAddress(String address){
  return new RegExp(r"^blacknet[a-z0-9]{59}$").hasMatch(address);
}

bool validatorBalance(String balance){
  return new RegExp(r"^([1-9]+|\d+.\d+)$").hasMatch(balance);
}