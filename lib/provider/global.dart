import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';

import 'package:blacknet/constants/index.dart';
import 'package:blacknet/theme/index.dart';
import 'package:blacknet/generated/i18n.dart';

class GlobalProvider with ChangeNotifier {
  bool _isAuthenticated = false;
  // inactive
  void syncPassCodeAuth(){
    notifyListeners();
  }
  void setPassCodeAuth(bool auth) {
    _isAuthenticated = !!auth;
    notifyListeners();
  }
  getPassCodeAuth() {
    return _isAuthenticated;
  }

  // language
  void syncLanguage(){
    String language = SpUtil.getString(Constants.language);
    if (language.isNotEmpty){
      notifyListeners();
    }
  }

  void setLanguage(Locale locale) {
    SpUtil.putString(Constants.language, S.delegate.title(locale));
    notifyListeners();
  }

  getLanguage() {
    String language = SpUtil.getString(Constants.language);
    if (!language.isNotEmpty){
      return S.delegate.getLocale(language);
    }
    return S.delegate.getLocale(language);
  }
  
  // theme
  void syncTheme(){
    String theme = SpUtil.getString(Constants.theme);
    if (theme.isNotEmpty){
      notifyListeners();
    }
  }

  void setTheme(Themes theme) {
    SpUtil.putString(Constants.theme, ThemeDatas.supportThemeMaps[theme]);
    notifyListeners();
  }

  getTheme() {
    return ThemeDatas.defaultThemeData;
    // String theme = SpUtil.getString(Constants.theme);
    // if (!theme.isNotEmpty){
    //   return ThemeDatas.supportThemeDatas[Constants.defaultTheme];
    // }
    // return ThemeDatas.supportThemeDatas[theme];
  }

  // inactive
  void syncInActive(){
    // int index = SpUtil.getInt("AppInActive", defValue: 0);
    notifyListeners();
  }
  void setInActive(int index) {
    SpUtil.putInt("AppInActive", index);
    notifyListeners();
  }
  getInActive() {
    return SpUtil.getInt("AppInActive", defValue: 0);
  }
}