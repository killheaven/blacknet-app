
import 'package:flutter/material.dart';
import 'package:blacknet/models/bln.dart';
import 'package:blacknet/db/txns.dart';
import 'package:blacknet/utils/index.dart';

class TxProvider with ChangeNotifier {

  TxDbHelper _txDb;

  Map<String, Map<int, List<BlnTxns>>> _mapTxList;

  Map<int, List<BlnTxns>> _blnTxList;

  TxProvider(){
      _mapTxList = Map();
      _txDb = TxDbHelper();
      _blnTxList = Map();
  }

  // bln txns

  Future<void> addTxlistAsync(List<BlnTxns> txs, [int type]) async{
    if(txs != null && txs.length > 0){
      await _txDb.insertUpdates(txs);
    }
    if(type == null || type == -1){
      addTxlist(txs, -1);
    }else{
      addTxlist(txs, -1);
      addTxlist(txs, type);
    }
  }

  void setTxlist(List<BlnTxns> txs, [int type]){
    if(type == null){
      type = -1;
    }
    _blnTxList[type] = txs;
    notifyListeners();
  }

  void addTxlist(List<BlnTxns> txs, [int type]){
    if(type == null){
      type = -1;
    }
    if(_blnTxList.containsKey(type)){
      // _blnTxList[type].addAll(txs);
      var list = getTxlist(type);
      if(list == null || list.isEmpty){
        setTxlist(txs, type);
        return;
      }
      var unique = Set<String>();
      var listSet = List<BlnTxns>();

      listSet.addAll(txs);
      listSet.addAll(list);

      listSet = listSet.where((l)=> unique.add(l.txid)).toList();

      try {
        listSet.sort((left, right)=> parseTime(right.time).microsecondsSinceEpoch.compareTo(parseTime(left.time).microsecondsSinceEpoch));
      } catch (e) {
        print(e);
      }

      setTxlist(listSet, type);
    }else{
      setTxlist(txs, type);
    }
  }

  List<BlnTxns> getTxlist([int type]){
    if(type == null){
      type = -1;
    }
    if(_blnTxList.containsKey(type)){
      return _blnTxList[type];
    }
    return null;
  }

  void updateTxlist(List<BlnTxns> txs, [int type]){
    if(type == null){
      type = -1;
    }
    if(txs == null || txs.isEmpty){
      return;
    }
    var list = getTxlist(type);
    if(list == null || list.isEmpty){
      setTxlist(txs, type);
      return;
    }
    var unique = Set<String>();
    var listSet = List<BlnTxns>();

    listSet.addAll(txs);

    listSet.addAll(list);

    listSet = listSet.where((l)=> unique.add(l.txid)).toList();

    try {
      listSet.sort((left, right)=> parseTime(right.time).microsecondsSinceEpoch.compareTo(parseTime(left.time).microsecondsSinceEpoch));
    } catch (e) {
      print(e);
    }
    
    setTxlist(listSet, type);
  }


  void notifyProloadPage(){
    notifyListeners();
  }

  Future<void> updateProloadPageAsync(String adddress, int type) async{
    // type == all
    if(type == null || type == -1){
      var alist = await getTxns(adddress, 1);
      setProloadPage(adddress, -1, alist);
    }
    // type 
    var list = await getTxns(adddress, 1, type);
    setProloadPage(adddress, type, list);
    notifyListeners();
  }

  void updateProloadPage(String adddress, int type){
    // type == all
    if(type == null || type == -1){
      getTxns(adddress, 1).then((list){
        setProloadPage(adddress, -1, list);
        notifyListeners();
      });
    }
    getTxns(adddress, 1, type).then((list){
      setProloadPage(adddress, type, list);
      notifyListeners();
    });
  }

  void addProloadPage(String adddress, int type, BlnTxns tx){
    _txDb.insertUpdate(tx).then((x){
      getTxns(adddress, 1, type).then((list){
        setProloadPage(adddress, type, list);
        notifyListeners();
      });
    });
  }

  Future<void> addProloadPageAsync(String adddress, int type, BlnTxns tx) async{
    await _txDb.insertUpdate(tx);
    await updateProloadPageAsync(adddress, type);
    notifyListeners();
  }

  void addProloadPages(String adddress, int type, List<BlnTxns> txs){
    _txDb.insertUpdates(txs).then((x){
      getTxns(adddress, 1, type).then((list){
        setProloadPage(adddress, type, list);
        notifyListeners();
      });
    });
  }

  Future<void> addProloadPagesAsync(String adddress, int type, List<BlnTxns> txs) async{
    await _txDb.insertUpdates(txs);
    await updateProloadPageAsync(adddress, type);
    notifyListeners();
  }

  void setProloadPage(String adddress, int type, List<BlnTxns> list){
    if(!_mapTxList.containsKey(adddress)){
      _mapTxList[adddress] = Map();
    }
    type = type == null ? -1 : type;
    _mapTxList[adddress][type] = list;
    notifyListeners();
  }

  List<BlnTxns> getProloadPages(String adddress, int type){
    if(_mapTxList.containsKey(adddress)){
      if(_mapTxList[adddress].containsKey(type)){
        return _mapTxList[adddress][type];
      }
    }
    return null;
  }

  // Txns
  Future<void>setTxns(List<BlnTxns> list) async{
    await _txDb.insertUpdates(list);
    notifyListeners();
  }
  
  Future<void>setTxnsAsync(List<BlnTxns> list) async{
    await _txDb.insertUpdates(list);
  }

  Future<List<BlnTxns>> getTxns(String address, [int page, int type]) async {
    if(type == -1 || type == null){
      return await _txDb.query(address, page);
    }else{
      return await _txDb.query(address, page, type);
    }
  }


}