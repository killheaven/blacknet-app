import 'package:blacknet/constants/index.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:blacknet/models/bln.dart';
import 'package:blacknet/utils/sp.dart';
import 'package:blacknet/db/txns.dart';

class AccountProvider with ChangeNotifier {
  TxDbHelper _txDb;

  AccountProvider(){
      _txDb = TxDbHelper();
  }
  
  // Txns
  Future<void>setTxns(List<BlnTxns> list) async{
    await _txDb.insertUpdates(list);
    notifyListeners();
  }
  
  Future<void>setTxnsAsync(List<BlnTxns> list) async{
    await _txDb.insertUpdates(list);
  }

  Future<List<BlnTxns>> getTxns(String address, [int page, int type]) async {
    if(type == -1 || type == null){
      return await _txDb.query(address, page);
    }else{
      return await _txDb.query(address, page, type);
    }
  }

  // cancellease
  void syncCancelLease(){
    var txStrs = SpUtil.getStringList("transfer_cancellease");
    if(txStrs != null && txStrs.isEmpty == false){
      notifyListeners();
    }
  }

  void setCancelLease(List<BlnLease> list) {
    SpUtil.putStringList("transfer_cancellease", list.map((i) => jsonEncode(i)).toList());
    notifyListeners();
  }

  void setCancelLeaseAsync(List<BlnLease> list) {
    SpUtil.putStringList("transfer_cancellease", list.map((i) => jsonEncode(i)).toList());
  }

  List<BlnLease> getCancelLease() {
    var txStrs = SpUtil.getStringList("transfer_cancellease");
    return txStrs.map((i)=> BlnLease.fromJson(jsonDecode(i))).toList();
  }

  // account
  void syncCurrentBln(){
    Bln bln = SpHelper.getObject<Bln>(Constants.currentAccount);
    if(bln != null){
      notifyListeners();
    }
  }

  void setCurrentBln(Bln bln) {
    SpUtil.putObject(Constants.currentAccount, bln);
    notifyListeners();
  }

  getCurrentBln() {
    Bln bln = SpHelper.getObject<Bln>(Constants.currentAccount);
    if(bln == null){
      bln = Bln.empty();
    }
    return bln;
  }

  BlnBalance getBalance() {
    BlnBalance balance = SpHelper.getObject<BlnBalance>(Constants.currentBalance);
    if(balance == null){
      balance = BlnBalance(balance: 0, stakingBalance: 0, confirmedBalance: 0, seq: 0);
    }
    return balance;
  }
  void refreshBalance(){
    BlnBalance balance = SpHelper.getObject<BlnBalance>(Constants.currentBalance);
    if(balance != null){
      notifyListeners();
    }
  }
  void setBalance(BlnBalance balance) {
    SpUtil.putObject(Constants.currentBalance, balance);
    notifyListeners();
  }
  void setBalanceAsync(BlnBalance balance) {
    SpUtil.putObject(Constants.currentBalance, balance);
  }
}