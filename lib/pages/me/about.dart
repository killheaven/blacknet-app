import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/tx.dart';
// router
import 'package:blacknet/routers/routers.dart';
// theme
import 'package:blacknet/theme/index.dart';
// widgets
import 'package:blacknet/widgets/index.dart';
// res
import 'package:blacknet/res/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// apis
import 'package:blacknet/components/api/api.dart';
import 'package:blacknet/components/index.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';
// constants
import 'package:blacknet/constants/index.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPage createState() => _AboutPage();
}

class _AboutPage extends State<AboutPage> with AutomaticKeepAliveClientMixin<AboutPage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => false;

  final TextEditingController _amountController = TextEditingController();
  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  final _formKey = GlobalKey<FormState>();
  
  double fee = 0.001;
  double amount = 1000;
  String to = Constants.donateAddress;
  String msg = Constants.donateMessage;
  bool encrypted = false;
  
  @override
  void initState() {
    super.initState();
  }

  void _forSubmitted(BuildContext context1) {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    BlnTxns _tx = BlnTxns();
    showDialog(
      context: context1,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return PasswordPayDialog(
          onValidation: (List<String> codes){
            if(bln.password == codes.join()){
              return true;
            }
            return false;
          },
          onCallback: (bool success){
            if(success){
              //tx
              _tx.data = BlnTxnsData();
              _tx.data.amount = (amount * 1e8).toDouble();
              _tx.from = bln.address;
              _tx.data.to = to;
              _tx.fee = (fee * 1e8).toInt();
              _tx.type = 0;
              _tx.time = (DateTime.now().microsecondsSinceEpoch/1e6).toInt().toString();
              //balance
              BlnBalance balance = Provider.of<AccountProvider>(context).getBalance();
              balance.balance -= _tx.data.amount.toInt();
              // balance notify
              Provider.of<AccountProvider>(context).setBalance(balance);
              // eventBus.fire(TxListRefreshEvent(_tx));
              Provider.of<TxProvider>(context).addTxlistAsync([_tx], _tx.type);
              // 跳转页面
              Navigator.pushNamed(context, Routes.txDetail,
                arguments: TxDetailPageArguments(
                  bln: _tx
                )
              );
            }
          },
          onPressed: () async{
            Api.transfer(bln.mnemonic, fee, amount, to, encrypted, msg).then((res){
              if(res.code == 200){
                _tx.txid = res.body;
                _verificationNotifier.add(true);
              }else{
                _verificationNotifier.add(false);
              }
            });
          },
          shouldTriggerVerification: _verificationNotifier.stream,
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).about),
        centerTitle: true
      ),
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 40,bottom: 30),
            child: Align(
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Container(
                    child: new Image(
                      image: new AssetImage('assets/images/logo.png')
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Text("Blacknet Mobile", style: TextStyle(fontSize: 18)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Text(Constants.version, style: TextStyle(fontSize: 18)),
                  )
                ],
              ),
            ),
          ),
          ListTile(
            leading: Text("Gitlab"),
            title: Text("https://gitlab.com/killheaven/blacknet-app.git", style: TextStyle(color: Colours.violet)),
            onTap: (){
              OkNavigator.webview(context, "Gitlab.com", "https://gitlab.com/killheaven/blacknet-app.git");
            },
          ),
          Container(
            margin: EdgeInsets.only(top: 40),
            padding: EdgeInsets.only(left: 40, right: 40),
            child: MyButton(
              key: Key("donate"),
              text: S.of(context).donate,
              onPressed: () async {
                  _amountController.text = "1000";
                  amount = await showDialog<double>(
                    context: context,
                    barrierDismissible: false, // user must tap button for close dialog!
                    builder: (BuildContext context) {
                      double realAmount = realBalance(Provider.of<AccountProvider>(context).getBalance().balance.toDouble());
                      return AlertDialog(
                        title: Text(S.of(context).donateTitle),
                        content: TextFormField(
                          key: Key("amount"),
                          autocorrect: true,
                          controller: _amountController,
                          autofocus: false,
                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                          style: TextStyle(
                            color: Theme.of(context).textTheme.display4.color
                          ),
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                            border: InputBorder.none,
                            filled: true,
                            fillColor: Theme.of(context).textTheme.body1.color,
                            hintText: S.of(context).amountInput
                          ),
                          validator: (value) {
                            if(!validatorBalance(value)){
                              return S.of(context).amountInputCorrect;
                            }
                            if(double.parse(value) > realAmount){
                              return S.of(context).amountInputEnough;
                            }
                            _amountController.text = value;
                            return null;
                          },
                          autovalidate: true
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(S.of(context).cancel),
                            onPressed: () {
                              Navigator.of(context).pop(0.0);
                            },
                          ),
                          FlatButton(
                            child: Text(S.of(context).donate),
                            onPressed: () {
                              try {
                                Navigator.of(context).pop(double.parse(_amountController.text));
                              } catch (e) {
                                print(e);
                              }
                            },
                          )
                        ],
                      );
                    },
                  );
                  if(amount > 0.0){
                    _forSubmitted(context);
                  }
              }
            )
          )
        ]
      )
    );
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    // pass
    _verificationNotifier.close();
    _amountController.dispose();
    super.dispose();
  }
}