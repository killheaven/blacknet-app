export 'package:blacknet/pages/account/index.dart';

export 'package:blacknet/pages/home/index.dart';
export 'package:blacknet/pages/theme/index.dart';
export 'package:blacknet/pages/transfer/index.dart';
export 'package:blacknet/pages/me/index.dart';

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:blacknet/components/password/index.dart';
// api
import 'package:blacknet/components/api/api.dart';
// privider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/global.dart';
import 'package:blacknet/provider/tx.dart';
// models
import 'package:blacknet/models/index.dart';
// home main page
import 'package:blacknet/pages/home/index.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';


class IndexPage extends StatefulWidget {
  @override
  _IndexPage createState() => _IndexPage();
}

class _IndexPage extends State<IndexPage> {
  // bln账号
  Bln bln = new Bln.empty();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    ScreenUtil.instance = ScreenUtil(width: 414, height: 896, allowFontScaling: true)..init(context);

    bln = Provider.of<AccountProvider>(context).getCurrentBln();

    // 判断账户是否存在显示创建账号页面
    if (bln.isEmpty()) {
      return Home();
    }else{
      return MainPage();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}