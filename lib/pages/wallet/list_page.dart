import 'package:blacknet/res/index.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:math' as math;
import "dart:collection";
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/pages/wallet/provider/list.dart';
import 'package:blacknet/provider/tx.dart';
// theme
import 'package:blacknet/theme/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';
// widget
import 'package:blacknet/widgets/index.dart';
// api
import 'package:blacknet/components/api/api.dart';
// event
import 'package:blacknet/events/events.dart';
// rooter
import 'package:blacknet/routers/routers.dart';

import 'package:flustars/flustars.dart';
// db ts
import 'package:blacknet/db/txns.dart';

class WalletList extends StatefulWidget {

  const WalletList({
    Key key,
    @required this.index,
    @required this.address,
    @required this.type,
  }): super(key: key);

  final int index;
  final int type;
  final String address;
  
  @override
  _WalletListState createState() => _WalletListState();
}

class _WalletListState extends State<WalletList> with AutomaticKeepAliveClientMixin<WalletList>{

  /// 是否正在加载数据
  bool _isLoading = false;
  int _page = 1;
  int _maxPage = 1;
  int _index = 0;
  int _type = 0;
  String _address;
  StateType _stateType = StateType.loading;
  ScrollController _controller = ScrollController();
  TxDbHelper _txDb = TxDbHelper();
  
  @override
  void initState() {
    super.initState();
    _index = widget.index;
    _type  = widget.type;
    _address = widget.address;
    // _address = "blacknet17tuwsx4twcl6ysjaj5pag03n59e56y37yagxht0rhmldl07yyxps6fhfjn";
    // _address = "blacknet126avvjlevw9xqnmlgscytxtuf57ndwzmz569vcaatceevhcjxjfq29ywqc";
    _initData();
    _onRefresh();
  }

  void _initData() async{
    List<BlnTxns> list;
    if(_type == -1){
      list = await _txDb.query(_address, 1);
    }else{
      list = await _txDb.query(_address, 1, _type);
    }
    if(mounted){
      Provider.of<TxProvider>(context).setTxlist(list, _type);
    }
  }

  @override
  Widget build(BuildContext context) {
    
    String emptyStr = S.of(context).noTransaction;
    switch (_type) {
      case 2:
        emptyStr = S.of(context).noOutLeases;
        break;
      case 254:
        emptyStr = S.of(context).noPosGenerated;
        break;
      default:
        emptyStr = S.of(context).noTransaction;
    }

    return NotificationListener(
      onNotification: (ScrollNotification note){
        if(note.metrics.pixels == note.metrics.maxScrollExtent){
          _loadMore();
        }
        return true;
      },
      child: RefreshIndicator(
        onRefresh: _onRefresh,
        displacement: 88.0, /// 默认40， 多添加的80为Header高度
        child: Consumer<WalletListProvider>(
          builder: (_, provider, child) {
            return CustomScrollView(
              /// 这里指定controller可以与外层NestedScrollView的滚动分离，避免一处滑动，5个Tab中的列表同步滑动。
              /// 这种方法的缺点是会重新layout列表
              controller: _index != provider.index ? _controller : null,
              key: PageStorageKey<String>("$_index"),
              slivers: <Widget>[
                SliverOverlapInjector(
                  ///SliverAppBar的expandedHeight高度,避免重叠
                  handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                ),
                child
              ],
            );
          },
          child: Consumer<TxProvider>(
            builder: (_, provider, __) {
              List<BlnTxns> list = provider.getTxlist(_type);
              return SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                sliver: list == null || list.isEmpty ? SliverFillRemaining(child: list != null && list.isEmpty ? StateLayout.empty(emptyStr):StateLayout(type: _stateType)) :
                SliverList(
                  delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                    return index < list.length ? WalletListItem(key: ValueKey<String>(list[index].txid), data: list[index], address: _address) : MoreWidget(list.length, _hasMore(), 100);
                    // return index < list.length ?  WalletListItem(data: list[index], address: _address) : MoreWidget(list.length, _hasMore(), 100);
                  },
                  childCount: list.length + 1
                  ),
                ),
              );
            }
          )
        ),
      ),
    );
  }

  List<BlnTxns> _list;

  Future _onRefresh() async {
    // balnace
    Api.getBlance(_address).then((balnace){
      if(mounted){
        Provider.of<AccountProvider>(context).setBalance(balnace);
      }
    });
    // tx
    _page = 1;
    var txs = await Api.getTxns(_address, _page, _type);
    if(mounted){
      if(txs.length == 100){
        ++_maxPage;
      }
      // add cache
      await _txDb.insertUpdates(txs);
      // 更新最新页面和缓存
      Provider.of<TxProvider>(context).updateTxlist(txs, _type);
    }
  }

  bool _hasMore(){
    return _page < _maxPage;
  }

  Future _loadMore() async {
    if (_isLoading) {
      return;
    }
    if (!_hasMore()){
      return;
    }
    _isLoading = true;
    var txs = await Api.getTxns(_address, ++_page, _type);

    // add list
    Provider.of<TxProvider>(context).addTxlist(txs, _type);
    
    setState(() {
      if(txs.length == 100){
        ++_maxPage;
      }
      _isLoading = false;
    });
  }
  
  @override
  bool get wantKeepAlive => true;
}

class WalletListItem extends StatefulWidget {
  const WalletListItem({
    Key key,
    this.data,
    this.address,
  }): super(key: key);

  final BlnTxns data;
  final String address;

  @override
  _WalletListItemState createState() => _WalletListItemState();
}

class _WalletListItemState extends State<WalletListItem> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> animation;
  BlnTxns data;
  String address;
  @override
  void initState() {
    super.initState();
    address = widget.address;
    data = widget.data;
    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    animation = Tween<double>(begin: 0, end: 1).animate(_controller)..addListener(() => setState(() {}));
    // _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // return Row(
    //   children: <Widget>[
    //     Expanded(
    //       flex: 10 - (animation.value * 10).toInt(),
    //       child: Gaps.empty,
    //     ),
    //     Expanded(
    //       flex: 50,
    //       child: _build()
    //     ),
    //     Expanded(
    //       flex: 10 - (animation.value * 10).toInt(),
    //       child: Gaps.empty,
    //     ),
    //   ],
    // );
    return _build();
  }

  bool _isReceive(){
    // pos
    if( address == data.from && data.type == 254){
      return true;
    }
    // transfer
    if( address != data.from && data.data.to == address){
      return true;
    }
    return false;
  }

  String _blnTypeName(){
    if(BlnTxData[data.type] == "transfer"){
      return _isReceive() ? S.of(context).transferReceived: S.of(context).transferSend;
    }
    return S.of(context).blnType(BlnTxData[data.type]);
  }

  Widget _build(){
    final isReceive = _isReceive();
    return InkWell(
      borderRadius:new BorderRadius.circular(20.0),//给水波纹也设置同样的圆角
      onTap: () {
        Navigator.pushNamed(context, Routes.txDetail,
          arguments: TxDetailPageArguments(
            bln: data
          )
        );
      },
      child: new Container(
        margin: EdgeInsets.only(left: 15, right: 15),
        padding: EdgeInsets.only(top: 12, bottom: 12),
        decoration: new BoxDecoration(
          border: Border(bottom: BorderSide(color: Colours.lighter, width: 0.5))
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                _blnTypeName(),
                // style: Theme.of(context).textTheme.display1,
                style: TextStyle(
                  color: Colours.blnType
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  (isReceive ? "+": "-")+ fomartBalance(data.data.amount, 8)+ " BLN",
                  overflow: TextOverflow.ellipsis,
                  // style: Theme.of(context).textTheme.display2,
                  style: TextStyle(
                    color: isReceive?Colours.blnRecevied:Colours.blnSent
                  ),
                )
              ),
            )
          ],
        )
      ),
    );
    // return Column(
    //   children: <Widget>[
    //     ListTile(
    //       dense: true,
    //       leading: Text(
    //         _blnTypeName(),
    //         style: Theme.of(context).textTheme.display1,
    //       ),
    //       trailing: Text(
    //         (isReceive ? "+": "-")+ fomartBalance(data.data.amount, 8),
    //         overflow: TextOverflow.ellipsis,
    //         style: Theme.of(context).textTheme.display2,
    //       ),
    //       onTap: () {
    //         Navigator.pushNamed(context, Routes.txDetail,
    //           arguments: TxDetailPageArguments(
    //             bln: data
    //           )
    //         );
    //       },
    //     ),
    //     Divider()
    //   ]
    // );
    // return new ListTile(
    //   title: Text(isFrom ? data.data.to != null ? shortAddress(data.data.to, 10) : shortAddress(data.from,10) : shortAddress(data.from,10)),
    //   subtitle: Text(formatDate(now)),
    //   leading: new CircleAvatar(child: new Icon(isPedding?  Icons.more_horiz : isFrom ? Icons.call_made : Icons.call_received)),
    //   trailing: new Column(
    //     crossAxisAlignment: CrossAxisAlignment.end,
    //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //     children: <Widget>[
    //       Padding(
    //         padding: ThemeLayout.paddingRight(10),
    //         child: Text(
    //           fomartBalance(data.data.amount),
    //           overflow: TextOverflow.ellipsis,
    //         )
    //       ),
    //       Text(
    //         S.of(context).blnType(BlnTxData[data.type])
    //       )
    //     ],
    //   ),
    //   onTap: () {
    //     Navigator.pushNamed(context, Routes.txDetail,
    //       arguments: TxDetailPageArguments(
    //         bln: data
    //       )
    //     );
    //   },
    // );
  }
}