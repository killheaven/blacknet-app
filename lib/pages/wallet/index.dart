import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/tx.dart';
import 'package:blacknet/pages/wallet/provider/list.dart';
// listpage
import 'package:blacknet/pages/wallet/list_page.dart';
// res
import 'package:blacknet/res/index.dart';
// theme
import 'package:blacknet/theme/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';

class WalletPage extends StatefulWidget {
  @override
  _WalletPage createState() => _WalletPage();
}

class _WalletPage extends State<WalletPage> with AutomaticKeepAliveClientMixin<WalletPage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => true;

  TabController _tabController;
  WalletListProvider provider = WalletListProvider();

  List<WalletPageTab> tabs = <WalletPageTab>[
      WalletPageTab(key: BlnTxType.all, title: 'all', icon: Icons.directions_car),
      WalletPageTab(key: BlnTxType.transfer, title: 'transfer', icon: Icons.directions_bike),
      WalletPageTab(key: BlnTxType.lease, title: 'lease', icon: Icons.directions_boat),
      WalletPageTab(key: BlnTxType.posGenerated, title: 'pos', icon: Icons.directions_bus),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: tabs.length);
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return ChangeNotifierProvider<WalletListProvider>(
      builder: (_) => provider,
      child: Scaffold(
        body: Stack(children: <Widget>[
          SafeArea(
              child: SizedBox(
                height: 0,
                width: double.infinity,
                child: const DecoratedBox(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [Colors.blue, Colors.blue])
                    )
                ),
              ),
          ),
          NestedScrollView(
            key: const Key('wallet_list'),
            physics: ClampingScrollPhysics(),
            headerSliverBuilder: (context, boxIsScrolled) {
              return _sliverBuilder(context);
            },
            body: PageView.builder(
                key: const Key('pageView'),
                itemCount: tabs.length,
                onPageChanged: _onPageChange,
                controller: _pageController,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (_, index) {
                  return WalletList(index: index, type: tabs[index].key, address: bln.address);
                }
            )
          )
        ])
      ),
    );
  }

  List<Widget> _sliverBuilder(BuildContext context) {
    return <Widget>[
      SliverOverlapAbsorber(
        handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
        child: SliverAppBar(
          leading: Gaps.empty,
          elevation: 0.0,
          centerTitle: true,
          expandedHeight: 120.0,
          floating: false, // 不随着滑动隐藏标题
          pinned: true, // 固定在顶部
          flexibleSpace: FlexibleSpaceBar(
            title: Text(fomartBalance(Provider.of<AccountProvider>(context).getBalance().balance.toDouble())+' BLN', style: TextStyle(color: Theme.of(context).accentColor)),
            centerTitle: true,
            // titlePadding: const EdgeInsetsDirectional.only(start: 16.0, bottom: 16.0),
            collapseMode: CollapseMode.pin, //视差效果
            background: Container(
              // color: Colors.redAccent,
              height: 120,
            ),
          ),
          // flexibleSpace: MyFlexibleSpaceBar(
          //   background: Container(
          //     // color: Colors.redAccent,
          //     height: 120,
          //   ),
          //   centerTitle: true,
          //   titlePadding: const EdgeInsetsDirectional.only(start: 16.0, bottom: 16.0),
          //   collapseMode: CollapseMode.pin,
          //   title: Text(fomartBalance(provider.balance)+' BLN'),
          // )
        ),
      ),
      SliverPersistentHeader(
        pinned: true,
        delegate: _SliverPersistentHeaderDelegate(
          TabBar(
            tabs: tabs.map((WalletPageTab tab) {
              return new Tab(
                text: S.of(context).blnType(tab.title)
              );
            }).toList(),
            controller: _tabController,
            //配置控制器
            isScrollable: false,
            // indicatorColor: Theme.of(context).canvasColor,
            indicatorWeight: 2,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.only(bottom: 0),
            labelPadding: EdgeInsets.only(left: 0),
            // labelColor: Theme.of(context).backgroundColor,
            labelStyle: TextStyle(
              fontSize: ThemeLayout.fontSize(15),
            ),
            // unselectedLabelColor: Theme.of(context).backgroundColor,
            unselectedLabelStyle: TextStyle(
              fontSize: ThemeLayout.fontSize(15),
            ),
            onTap: (index){
              if (!mounted){
                return;
              }
              _pageController.jumpToPage(index);
            },
          )
        ),
      )
    ];
  }

  PageController _pageController = PageController(initialPage: 0);
  _onPageChange(int index) async{
    provider.setIndex(index);
    _tabController.animateTo(index);
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}

class _SliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {

  final TabBar _tabBar;
  
  _SliverPersistentHeaderDelegate(this._tabBar);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      color: Theme.of(context).primaryColor,
      child: _tabBar,
    );
  }

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
  
}

class SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final Widget widget;
  final double height;
  SliverAppBarDelegate(this.widget, this.height);

  // minHeight 和 maxHeight 的值设置为相同时，header就不会收缩了
  @override
  double get minExtent => height;

  @override
  double get maxExtent => height;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return widget;
  }

  @override
  bool shouldRebuild(SliverAppBarDelegate oldDelegate) {
    return true;
  }
}

class WalletPageTab {
  WalletPageTab({ this.title, this.icon , this.key});
  String title;
  int key;
  IconData icon;
}