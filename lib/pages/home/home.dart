import 'package:flutter/material.dart';
import 'package:blacknet/routers/routers.dart';
// theme
import 'package:blacknet/theme/index.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';

// widgets
import 'package:blacknet/widgets/index.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new Expanded(
              flex: 2,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: ThemeLayout.width(160),
                    height: ThemeLayout.width(160),
                    child: new Image.asset(
                      'assets/images/logo.png',
                    )
                  ),
                  Container(
                    margin: ThemeLayout.marginTop(20),
                    child: new Text(S.of(context).name, style: new TextStyle(
                      fontSize: ThemeLayout.fontSize(24),
                    ))
                  )
                ],
              )
            ),
            new Expanded(
              flex: 2,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Expanded(
                    flex: 2,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: ThemeLayout.width(300),
                          padding: ThemeLayout.edgeInsetsAll(10),
                          child: MyButton(
                            key: Key("import"),
                            text: S.of(context).importWallet,
                            onPressed: () async {
                              Routes.router.navigateTo(context, Routes.accountImport1);
                            }
                          )
                        ),
                        SizedBox(height: ThemeLayout.height(20)),
                        Container(
                          width: ThemeLayout.width(300),
                          padding: ThemeLayout.edgeInsetsAll(10.0),
                          child: MyButton(
                            key: Key("create"),
                            text: S.of(context).craeteWallet,
                            onPressed: () async {
                              Routes.router.navigateTo(context, Routes.accountCreate);
                            }
                          )
                        )
                      ],
                    )
                  )
                ],
              ),
            )
          ]
        )
      )
    );
  }
}