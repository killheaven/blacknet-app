import 'dart:async';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/routers/routers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/tx.dart';
import 'package:blacknet/provider/account.dart';
// theme
import 'package:blacknet/theme/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// apis
import 'package:blacknet/components/api/api.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';
// widgets
import 'package:blacknet/widgets/index.dart';
// 
import 'package:flustars/flustars.dart';
import 'package:flutter/services.dart';
// event
import 'package:blacknet/events/events.dart';

class TxDetailPage extends StatefulWidget {
  @override
  _TxDetailPage createState() => _TxDetailPage();
}

class _TxDetailPage extends State<TxDetailPage> with AutomaticKeepAliveClientMixin<TxDetailPage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => true;

  ScrollController _scrollController = ScrollController();
  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  StateType _stateType = StateType.empty;

  BlnTxns _tx;

  BlnTxns _txnew;

  String _txhash;

  bool _isPending = false;
  
  @override
  void initState() {
    super.initState();
  }

  bool _isReceive(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    // pos
    if( bln.address == _tx.from && _tx.type == 254){
      return true;
    }
    // transfer
    if( bln.address != _tx.from && _tx.data.to == bln.address){
      return true;
    }
    return false;
  }

  // 刷新
  Future<Null> _onRefresh() async {
    if(_txhash == null){
      return;
    }
    var tx = await Api.getTx(_txhash);
    if(mounted){
      setState(() {
        if(tx.txid != null){
          _txnew = tx;
          // eventBus.fire(TxListRefreshEvent(_txnew));
          Provider.of<TxProvider>(context).addTxlistAsync([tx], tx.type);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    TxDetailPageArguments args = ModalRoute.of(context).settings.arguments;
    _tx = args.bln;
    _tx.hash = _tx.txid;
    _txhash = _tx.txid;
    if(_txnew != null){
      _tx = _txnew;
    }
    _isPending = false;
    if(_tx.seq == null || _tx.data == null || (_tx.data != null && _tx.data.blockHeight == null)){
      // 定时刷新获取数据
      _isPending = true;
      _onRefresh();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).blnType(BlnTxData[_tx.type])),
        elevation: 0,
        centerTitle: true,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: NotificationListener(
              child: RefreshIndicator(
                //下拉刷新触发方法
                onRefresh: _onRefresh,
                child: CustomScrollView(
                  controller: _scrollController,
                  key: PageStorageKey<String>("txdetail"),
                  physics: AlwaysScrollableScrollPhysics(),
                  slivers: <Widget>[
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 0.0),
                      sliver: _stateType == StateType.loading ? SliverFillRemaining(child: StateLayout(type: _stateType)) :
                      SliverList(
                        delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                          return index < 1 ?  _buildCard(context) : MoreWidget(1, false, 1000000000);
                        },
                        childCount: 2),
                      ),
                    )
                  ],
                )
              )
            )
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 40),
            child: InkWell(
              child: Center(child: Text(S.of(context).detailBottomText, style: TextStyle(color: Colours.violet))),
              onTap: (){
                OkNavigator.webview(context, "Blnscan.io Blacknet Explorer", _tx.type == 254 ? "https://blnscan.io/"+ _tx.from: "https://blnscan.io/"+ _tx.txid);
              },
            ),
            // child: Center(child: Text(S.of(context).detailBottomText))
          )
        ]
      )
    );
  }

  Widget _buildCard(BuildContext context) {
    final isReceive = _isReceive();
    DateTime now = _tx.time == null ? DateTime.now() : parseTime(_tx.time);
    return Container(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
      child: new Card(
        color: Theme.of(context).canvasColor,
        elevation: 0,  //设置阴影
        // shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(14.0))),  //设置圆角
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: new Column(  // card只能有一个widget，但这个widget内容可以包含其他的widget
            children: [
              new ListTile(
                contentPadding: const EdgeInsets.only(top: 20),
                title: new Column( 
                  children: <Widget>[
                    Center(child: Container(
                      width: 60,
                      height: 60,
                      child: Icon(_isPending? Icons.more_horiz :Icons.check, size: 60),
                    )),
                    // Center(child: _isPending ? Text('转账中') : Text('转账成功'))
                  ],
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Center(child: Text(formatDate(now)))
                )
              ),
              // new Divider(),
              new ListTile(
                title: new Text((isReceive ? "+": "-")+ fomartBalance(_tx.data.amount, 8)+ " BLN", style: TextStyle(
                    color: isReceive?Colours.blnRecevied:Colours.blnSent
                )),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).amount),
                )
              ),
              _tx.fee > 0 ? new ListTile(
                title: new Text(fomartBalance(_tx.fee.toDouble())+ ' BLN', style: TextStyle(color: Colours.gold)),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).fee)
                )
              ) : Gaps.empty,
              _buildTo(_tx),
              _buildFrom(_tx),
              // new Divider(),
              _tx.txid != null ? new ListTile(
                title: Wrap(
                  spacing: 2.0,
                  children: <Widget>[
                    Text(_tx.txid != null ? _tx.txid.toString() : "", style: TextStyle(color: Colours.violet)),
                    // Icon(Icons.content_copy, size: 14)
                  ],
                ),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).hash),
                ),
                onTap: _tx.txid == null ? null : (){
                  OkNavigator.webview(context, "Blnscan.io Blacknet Explorer", "https://blnscan.io/"+ _tx.txid);
                }
              ): Gaps.empty,
              new ListTile(
                title: _tx.data.blockHeight != null ? Text(_tx.data.blockHeight.toString(), style: TextStyle(color: Colours.violet)) : Align(alignment: Alignment.centerLeft, child: const CupertinoActivityIndicator()),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).block),
                ),
                onTap: _tx.data.blockHeight == null ? null : (){
                  OkNavigator.webview(context, "Blnscan.io Blacknet Explorer", "https://blnscan.io/"+ _tx.data.blockHeight.toString());
                }
              ),
            ],
          )
        )
      )
    );
  }

  Widget _buildTo(BlnTxns _tx){
    var to = _tx.data.to != null ? _tx.data.to.toString() : "";
    if(_tx.type == 254){
      to = _tx.from;
    }
    return new ListTile(
      title: Wrap(
        spacing: 2.0,
        children: <Widget>[
          Text(to,style: TextStyle(color: Colours.violet)),
          // Icon(Icons.content_copy, size: 14)
        ]
      ),
      leading: Container(
        width: 70,
        child: Text(S.of(context).toAddress),
      ),
      onTap: (){
        OkNavigator.webview(context, "Blnscan.io Blacknet Explorer", "https://blnscan.io/"+ to);
      }
    );
  }

  Widget _buildFrom(BlnTxns _tx){
    var from = _tx.from != null ? _tx.from.toString() : "";
    if(_tx.type == 254){
      from = S.of(context).posGenerated;
    }
    return new ListTile(
      title: Wrap(
        spacing: 2.0,
        children: <Widget>[
          _tx.type == 254 ? Text(from) :Text(from,style: TextStyle(color: Colours.violet))
          // _tx.type == 254 ? Gaps.empty :Icon(Icons.content_copy, size: 14)
        ],
      ),
      leading: Container(
        width: 70,
        child: Text(S.of(context).fromAddress),
      ),
      onTap: _tx.type == 254 ? null :(){
        OkNavigator.webview(context, "Blnscan.io Blacknet Explorer", "https://blnscan.io/"+ from);
      }
    );
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
}