import 'package:blacknet/widgets/index.dart';
import 'package:flutter/material.dart';
// privider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';

// i8n
import 'package:blacknet/generated/i18n.dart';
// router
import 'package:blacknet/routers/routers.dart';
// utils
import 'package:blacknet/utils/index.dart';
// model
import 'package:blacknet/models/index.dart';
// widget
import 'package:blacknet/pages/account/widgets/password.dart';

class AccountCreatePage2 extends StatefulWidget {
  @override
  _AccountCreatePage2 createState() => _AccountCreatePage2();
}

class _AccountCreatePage2 extends State<AccountCreatePage2> {
  
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AccountPageArguments args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(S.of(context).craeteWallet),
            Text(S.of(context).createStep2, style: TextStyle(fontSize: 14))
          ],
        )
      ),
      body: PasswordSettingWidget(
        text: <Widget>[
          Text(S.of(context).confirmPassowrd, style: TextStyle(fontSize: 20))
        ],
        buttonText: S.of(context).done,
        onPressed: (List<String> codes) async{
          if(args.password == codes.join('')){
            OkProgressDialog.show(S.of(context).walletCreating);
            Bln bln = await createBln();
            await Future.delayed(Duration(milliseconds: 100));
            OkProgressDialog.hide();
            bln.password = args.password;
            Provider.of<AccountProvider>(context).setCurrentBln(bln);
            Routes.router.navigateTo(context, Routes.accountBackup, clearStack: false);
          }else{
            Toast.show(S.of(context).inconsistentPassowrd);
          }
        },
      )
    );
  }

  Future<Bln> createBln() async {
      await Future.delayed(Duration(milliseconds: 500));
      Bln bln = new Bln();
      return bln;
  }
}