import 'package:blacknet/widgets/index.dart';
import 'package:flutter/material.dart';
// privider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';

// i8n
import 'package:blacknet/generated/i18n.dart';
// router
import 'package:blacknet/routers/routers.dart';
// utils
import 'package:blacknet/utils/index.dart';
// model
import 'package:blacknet/models/index.dart';

import 'package:blacknet/pages/account/widgets/password.dart';

class AccountRecoverPage3 extends StatefulWidget {
  @override
  _AccountRecoverPage3 createState() => _AccountRecoverPage3();
}

class _AccountRecoverPage3 extends State<AccountRecoverPage3> {
  bool _loading = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AccountPageArguments args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(S.of(context).importWallet),
            Text(S.of(context).importStep3, style: TextStyle(fontSize: 14))
          ],
        )
      ),
      body: PasswordSettingWidget(
        text: <Widget>[
          Text(S.of(context).confirmPassowrd, style: TextStyle(fontSize: 20))
        ],
        buttonText: S.of(context).done,
        onPressed: (List<String> codes) async{
          if(args.password == codes.join('')){
            OkProgressDialog.show(S.of(context).walletCreating);
            Bln bln = Bln.from(mnemonic: args.mnemonic, address: args.address);
            bln.password = args.password;
            await Future.delayed(Duration(milliseconds: 500));
            Provider.of<AccountProvider>(context).setCurrentBln(bln);
            OkProgressDialog.hide();
            Routes.router.navigateTo(context, Routes.index, clearStack: true);
          }else{
            Toast.show(S.of(context).inconsistentPassowrd);
          }
        },
      )
    );
  }
}