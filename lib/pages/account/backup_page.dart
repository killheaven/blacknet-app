import 'package:fluro/fluro.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
// privider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/global.dart';
// theme
import 'package:blacknet/theme/index.dart';
// models
import 'package:blacknet/models/index.dart';
// i8n
import 'package:blacknet/generated/i18n.dart';
// utils
import 'package:blacknet/utils/toast.dart';

// ruters
import 'package:blacknet/routers/routers.dart';
import 'package:blacknet/widgets/index.dart';

class AccountBackupPage extends StatefulWidget {
  @override
  _AccountBackupPage createState() => _AccountBackupPage();
}

class _AccountBackupPage extends State<AccountBackupPage> {
  Bln bln;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(S.of(context).backupMnemonic),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () { Routes.router.navigateTo(context, Routes.index, clearStack: true, transition: TransitionType.inFromLeft); },
              iconSize: 24,
            );
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Card(
              margin: const EdgeInsets.only(top: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    trailing: Icon(Icons.content_copy, color: Theme.of(context).accentColor, size: 16),
                    title: Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(S.of(context).mnemonic),
                    ),
                    subtitle: Container(
                      padding: const EdgeInsets.only(top: 10,bottom: 10),
                      child: Text(bln.mnemonic),
                    ),
                    onTap: (){
                      Clipboard.setData(new ClipboardData(text: bln.mnemonic)).then((v){
                        Toast.show(S.of(context).copySuccess);
                      });
                    },
                  )
                ],
              ),
            ),
            Card(
              margin: const EdgeInsets.only(top: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    trailing: Icon(Icons.content_copy, color: Theme.of(context).accentColor, size: 16),
                    title: Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(S.of(context).address),
                    ),
                    subtitle: Container(
                      padding: const EdgeInsets.only(top: 10,bottom: 10),
                      child: Text(bln.address),
                    ),
                    onTap: (){
                      Clipboard.setData(new ClipboardData(text: bln.address)).then((v){
                        Toast.show(S.of(context).copySuccess);
                      });
                    },
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 25),
              child: MyButton(
                key: Key("use"),
                text: S.of(context).done,
                onPressed: () async {
                  Routes.router.navigateTo(context, Routes.index, clearStack: true, transition: TransitionType.inFromLeft);
                }
              )
            )
          ],
        )
      )
    );
  }
}