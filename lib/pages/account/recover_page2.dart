import 'package:flutter/material.dart';
// routers
import 'package:blacknet/routers/routers.dart';
// i8n
import 'package:blacknet/generated/i18n.dart';

import 'package:blacknet/pages/account/widgets/password.dart';

class AccountRecoverPage2 extends StatefulWidget {
  @override
  _AccountRecoverPage2 createState() => _AccountRecoverPage2();
}

class _AccountRecoverPage2 extends State<AccountRecoverPage2> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AccountPageArguments args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(S.of(context).importWallet),
            Text(S.of(context).importStep2, style: TextStyle(fontSize: 14))
          ],
        )
      ),
      // backgroundColor: Theme.of(context).accentColor,
      body: PasswordSettingWidget(
        text: <Widget>[
          Text(S.of(context).settingPassowrd, style: TextStyle(fontSize: 20))
        ],
        buttonText: S.of(context).nextStep,
        onPressed: (List<String> codes){
          Navigator.pushNamed(context, Routes.accountImport3,
            arguments: AccountPageArguments(
              password: codes.join(''),
              mnemonic: args.mnemonic,
              address: args.address
            ));
        },
      )
    );
  }
}